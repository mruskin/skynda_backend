package me.skynda.common.dto;

import lombok.Data;

@Data
public class DeleteResponseDto {
    private boolean success;
}
