package me.skynda.common.interfaces.daos;

import me.skynda.common.db.SkyndaBaseEntityDao;
import me.skynda.image.entities.Image;

public interface ImageDao extends SkyndaBaseEntityDao<Image> {
}
