package me.skynda.common.interfaces.daos;

import me.skynda.common.db.SkyndaBaseEntityDao;
import me.skynda.vehicle.entities.VehicleReview;

public interface VehicleReviewDao extends SkyndaBaseEntityDao<VehicleReview> {
}
