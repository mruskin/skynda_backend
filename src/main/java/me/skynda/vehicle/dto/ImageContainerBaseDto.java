package me.skynda.vehicle.dto;

import me.skynda.vehicle.dto.ImageDto;

public interface ImageContainerBaseDto {

    ImageDto getImage();
}
