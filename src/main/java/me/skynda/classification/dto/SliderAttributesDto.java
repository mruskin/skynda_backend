package me.skynda.classification.dto;

import lombok.Data;

@Data
public class SliderAttributesDto extends ClassificationBaseDto {
    public int Min;
    public int Max;
}
